
package jp.co.kayo.android.localplayer.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.media.audiofx.PresetReverb;
import android.media.audiofx.Visualizer;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import jp.co.kayo.android.localplayer.BaseFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.adapter.StringListAdapter;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.core.VerticalSeekBar;
import jp.co.kayo.android.localplayer.core.VerticalSeekBar.OnSeekBarChangeListener;
import jp.co.kayo.android.localplayer.plugin.AudioVisualizer;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;

@SuppressLint("NewApi")
public class EqualizerFragment extends BaseFragment implements ContentManager,
        OnSeekBarChangeListener, OnTouchListener, OnGestureListener {
    private static final String EQUALIZER_MODE_KEY = "equalizer.mode.key";
    private Visualizer mVisualizer;
    int mAudioSessionId = -1;

    AudioVisualizer mVisualizerView;
    SharedPreferences mPref;

    LinearLayout seekLayout;
    AudioManager audiomgr;
    TextView[] textCurHz = null;
    TextView[] textBands = null;
    VerticalSeekBar[] seekBands = null;
    View[] trackview = null;
    TextView textCurVol;
    TextView textCurBass;
    TextView textCurVirtual;
    VerticalSeekBar seekVolume;
    VerticalSeekBar seekBassStrengs;
    VerticalSeekBar seekVirtualStrengs;
    CheckBox chkEqOn;
    CheckBox chkBsOn;
    CheckBox chkVrOn;
    CheckBox chkRvOn;
    int minEQLevel;
    int maxEQLevel;
    Spinner spinPreset;
    Spinner spinReverb;
    short[] REVERB_PRESET = new short[] {
            PresetReverb.PRESET_NONE,
            PresetReverb.PRESET_PLATE, PresetReverb.PRESET_SMALLROOM,
            PresetReverb.PRESET_MEDIUMHALL, PresetReverb.PRESET_MEDIUMROOM,
            PresetReverb.PRESET_LARGEHALL, PresetReverb.PRESET_LARGEROOM
    };

    Handler mHandler = new Handler();
    private GestureDetector mGestureDetector;
    private int mModeFft;
    public List<FrqData> mFrqDataList = new ArrayList<FrqData>();
    private List<FrqInfo> mFrqInfoList = null;

    private byte[] mFFTData = null;
    private int mMaxVol;
    private int mSamplingRate;
    private float mThGain = 1.1f;// 1.0f;//この数値を大きくするほど検出する音が少なくなる

    public interface VisualMode {
        public static final int FFT = 0;
        public static final int WAVE = 1;
        public static final int BMS = 2;
    }

    int N = 2;
    int CUT = 0;
    float[][] sumsf = new float[N][512];
    long sumcount = 0;
    float[] actf = new float[512];

    private long mTime;
    
    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    boolean isEnableVisual() {
        IMediaPlayerService binder = getBinder();
        try {
            if (binder != null
                    && (binder.stat() & AppWidgetHelper.FLG_PLAY) != 0) {
                return true;
            }
        } catch (RemoteException e) {
        }

        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        IMediaPlayerService binder = getBinder();
        if (binder == null) {
            chkEqOn.setEnabled(false);
            spinPreset.setEnabled(false);
            spinReverb.setEnabled(false);
            chkBsOn.setEnabled(false);
            chkVrOn.setEnabled(false);
            chkRvOn.setEnabled(false);
            if (seekBands != null) {
                for (int i = 0; i < seekBands.length; i++) {
                    seekBands[i].setEnabled(false);
                }
            }
        } else {
            try {
                mAudioSessionId = binder.getAudioSessionId();
                if (mAudioSessionId != -1) {
                    setEqualizer(binder);
                }
            } catch (RemoteException e) {
            }
        }

        bind();

        if (mVisualizerView != null) {
            mVisualizerView.calcDimension();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mVisualizer != null) {
            mVisualizer.setEnabled(false);
            mVisualizer.release();
            mVisualizer = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearBmsData();
    }

    private void clearBmsData() {
        if (mFrqInfoList == null) {
            return;
        }
        for (int i = 0; i < mFrqInfoList.size(); i++) {
            FrqInfo info = mFrqInfoList.get(i);
            info.reset();
            info.mAttackTimelist.clear();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EQUALIZER_MODE_KEY, mModeFft);
    }
    
    public void savePreset(int sel) {

        JSONObject json = new JSONObject();
        try {
            json.put("eqtype", spinPreset.getSelectedItemPosition());
            json.put("rvtype", spinReverb.getSelectedItemPosition());
            if (seekBassStrengs != null){
                json.put("bass", seekBassStrengs.getProgress());
            }
            if(seekVirtualStrengs != null){
                json.put("virtual", seekVirtualStrengs.getProgress());
            }
            if (seekBands != null) {
                for (int i = 0; i < seekBands.length; i++) {
                    json.put("band" + i, seekBands[i].getProgress());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Editor editor = mPref.edit();
        editor.putString("key.preset" + sel, json.toString());
        editor.commit();

        Toast.makeText(getActivity(), String.format(getString(R.string.txt_save_preset_msg), sel),
                Toast.LENGTH_SHORT).show();
    }

    public void loadPreset(int sel) {
        String jsonstr = mPref.getString("key.preset" + sel, "");
        if (jsonstr.length() > 0) {
            try {
                JSONObject json = new JSONObject(jsonstr);
                int eqtype = Funcs.parseInt(json.getString("eqtype"));
                int rvtype = Funcs.parseInt(json.getString("rvtype"));
                
                int bass = Funcs.parseInt(json.getString("bass"));
                int virtual = Funcs.parseInt(json.getString("virtual"));
                getBinder().setStrength(bass);
                seekBassStrengs.setProgress(bass);
                getBinder().setVirtual(virtual);
                seekVirtualStrengs.setProgress(virtual);
                if (eqtype == 0 && seekBands != null) {
                    for (int i = 0; i < seekBands.length; i++) {
                        int lv = Funcs.parseInt(json.getString("band" + i));
                        int value = lv - Math.abs(minEQLevel);
                        getBinder().setBandLevel(i, value);
                        seekBands[i].setProgress(lv);
                    }
                }else{
                    spinPreset.setSelection(eqtype);
                }
                spinReverb.setSelection(rvtype);

                Toast.makeText(getActivity(), String.format(getString(R.string.txt_load_preset_msg), sel),
                        Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private void setEqualizer(IMediaPlayerService binder) {
        if (binder != null) {
            try{
                createSeekBar(binder);
                // enable status
                chkEqOn.setEnabled(true);
                chkBsOn.setEnabled(true);
                chkVrOn.setEnabled(true);
                chkRvOn.setEnabled(true);
                spinPreset.setEnabled(true);
                spinReverb.setEnabled(true);
    
                // Equalizer Spinner
                if ( spinPreset.getAdapter() == null) {
                    int pn = binder.getNumberOfPresets();
                    List<String> values_eq = new ArrayList<String>();
                    if (pn > 0) {
                        values_eq.add(getString(R.string.lb_preset_custmize));
                        for (short i = 0; i < pn; i++) {
                            String name = binder.getPresetName(i);
                            values_eq.add(name);
                        }
                        StringListAdapter eqAdapter = new StringListAdapter(getActivity(), values_eq);
                        spinPreset.setAdapter(eqAdapter);
                    }
                }
                if(spinPreset.getAdapter() != null){
                    int cur = binder.getCurrentPreset() + 1;
                    spinPreset.setSelection(cur);
                }
    
                // Reverb Spinner
                if(spinReverb.getAdapter() == null){
                    String[] presetReverb = getResources().getStringArray(
                            R.array.listtype_preset_reverb);
                    List<String> values_rv = new ArrayList<String>();
                    for (String s : presetReverb) {
                        values_rv.add(s);
                    }
                    StringListAdapter reverbAdapter = new StringListAdapter(getActivity(), values_rv);
                    spinReverb.setAdapter(reverbAdapter);
                }
    
                if(spinReverb.getAdapter()!=null){
                    int cur = binder.getRvPreset();
                    for (int i = 0; i < REVERB_PRESET.length; i++) {
                        if (REVERB_PRESET[i] == cur) {
                            spinReverb.setSelection(i);
                            break;
                        }
                    }
                }
    
                // Enable
                chkEqOn.setChecked(binder.getEqEnabled());
                chkBsOn.setChecked(binder.getBsEnabled());
                chkVrOn.setChecked(binder.getVrEnabled());
                chkRvOn.setChecked(binder.getRvEnabled());
    
                // bands
                int bands = binder.getNumberOfBands();
                if (bands > 0) {
                    minEQLevel = binder.getMinEQLevel();
                    maxEQLevel = binder.getMaxEQLevel();
                    for (int i = 0; i < bands; i++) {
                        seekBands[i].setMax(maxEQLevel + Math.abs(minEQLevel));
                        int n = binder.getCenterFreq(i);
                        textBands[i].setText(getTextHz(n / 1000));
                        int x = binder.getBandLevel(i);
                        seekBands[i].setProgress(x + Math.abs(minEQLevel));
                        textCurHz[i].setText(Integer.toString(x));
                        seekBands[i].setEnabled(true);
                    }
    
                    int str = binder.getStrength();
                    seekBassStrengs.setProgress(str);
                    textCurBass.setText(Integer.toString(str));
                    
                    int virtual = binder.getVirtual();
                    seekVirtualStrengs.setProgress(virtual);
                    textCurVirtual.setText(Integer.toString(virtual));
                }
            }
            catch(RemoteException e){
                Logger.e("イコライザーのコンボボックス設定で例外", e);
            }
        }
    }

    private String getTextHz(int n) {
        if (n < 1000) {
            return Integer.toString(n) + "Hz";
        } else {
            return Integer.toString(n / 1000) + "kHz";
        }
    }

    private void bind() {
        if (mAudioSessionId != -1) {
            // Visualize
            sumcount = 0;
            try {
                if (mVisualizer != null) {
                    mVisualizer.setEnabled(false);
                    mVisualizer.release();
                }
                mVisualizer = new Visualizer(mAudioSessionId);
                // 音声データをキャプチャするサイズを設定
                int captureSizeRange = Visualizer.getCaptureSizeRange()[1];
                int captureRate = Visualizer.getMaxCaptureRate();
                mVisualizer.setCaptureSize(captureSizeRange);
                // キャプチャしたデータを定期的に取得するリスナーを設定
                mVisualizer.setDataCaptureListener(
                        new Visualizer.OnDataCaptureListener() {
                            // Wave形式のキャプチャーデータ
                            public void onWaveFormDataCapture(
                                    Visualizer visualizer, final byte[] bytes,
                                    int samplingRate) {
                                mVisualizerView.updateVisualizerWave(bytes);

                            }

                            // 高速フーリエ変換のキャプチャーデータ
                            public void onFftDataCapture(Visualizer visualizer,
                                    final byte[] bytes, int samplingRate) {
                                mSamplingRate = samplingRate / 1000;
                                setFFTData(bytes);
                            }
                        }, captureRate, // キャプチャーデータの取得レート（ミリヘルツ）
                        mModeFft == VisualMode.WAVE,// これがTrueだとonWaveFormDataCaptureにとんでくる
                        mModeFft != VisualMode.WAVE);// これがTrueだとonFftDataCaptureにとんでくる
                mVisualizer.setEnabled(true);
            } catch (UnsupportedOperationException e) {
                Logger.e("UnsupportedOperationException", e);
            } catch (RuntimeException e) {
                Logger.e("RuntimeException", e);
            }
        }
    }

    // FFTデータの周波数全範囲の増加量を記憶する
    public class FrqData {
        public int mCount = 0;
        // public float mLastAdd = 0;
        public float mTotalAdd = 0;

        public int mAvgCount = 0;
        public float mAvg = 0.5f;
        public long mLastCountTime = 0;
        // public float mLastCountValue = 0;

        public float mAvg_dt = 0;
        public float mDev_avg = 0;
        public int mErrorCount = 0;

        public long mBaseTime = 0;
        public float mAtack = 0;

    }

    // 音が鳴った時刻を記憶
    public class AttackTime {
        public long mTime = 0;
        public float mValue = 0f;

        public AttackTime(long t, float v) {
            mTime = t;
            mValue = v;
            if (mValue < 0)
                mValue = 0;
            if (mValue > 1)
                mValue = 1;
        }
    }

    // 各周波数毎の解析情報
    public class FrqInfo {
        public float mFrq = 0;
        public int mFrqWidth = 1;
        public float mTh = 0.5f;
        public int mResul = -1;
        public int mPos = 0;
        public List<AttackTime> mAttackTimelist = new ArrayList<AttackTime>();

        public FrqInfo(float frq, float th, int w) {
            mFrq = frq;
            mTh = th;
            mFrqWidth = w;
        }

        public void reset() {
            mResul = -1;
            mPos = 0;
            mAttackTimelist.clear();
        }
    }

    // FFTデータの解析
    public void setFFTData(byte[] fftData) {
        IMediaPlayerService binder = getBinder();
        if (binder == null) {
            return;
        }
        if ((mModeFft == VisualMode.BMS) && (fftData != null)) {
            try {
                mTime = binder.getSeekPosition();
            } catch (RemoteException e) {
            }
            int n = fftData.length;
            if (n > 0) {
                if (mFrqDataList.size() != n / 2) {
                    for (int i = 0; i < n / 2; i++) {
                        mFrqDataList.add(new FrqData());
                    }
                } else if (mFFTData != null) {
                    float vol = (float) getCurrentVol();
                    float valueGain = vol <= 0 ? 1 : mMaxVol / vol;
                    for (int i = 0; i < n / 2; i++) {
                        FrqData frqData = mFrqDataList.get(i);
                        byte rvale0 = mFFTData[2 * i];
                        byte ivalue0 = mFFTData[2 * i + 1];
                        byte rvale1 = fftData[2 * i];
                        byte ivalue1 = fftData[2 * i + 1];
                        float value0 = valueGain
                                * (float) Math.sqrt(rvale0 * rvale0 + ivalue0
                                        * ivalue0) / 255;
                        float value1 = valueGain
                                * (float) Math.sqrt(rvale1 * rvale1 + ivalue1
                                        * ivalue1) / 255;
                        float add = value1 - value0;

                        if (add != 0) {
                            // 音の強さの平均を求める
                            frqData.mAvgCount++;
                            frqData.mAvg = (frqData.mAvg
                                    * (frqData.mAvgCount - 1) + value1)
                                    / (float) frqData.mAvgCount;
                        }
                        if (value1 > frqData.mAvg * 1.1) {
                            if (frqData.mAtack <= 0) {
                                frqData.mCount++;

                                // 前回からの時間差
                                float dt = mTime - frqData.mLastCountTime;

                                // 時間差の平均
                                frqData.mAvg_dt = (frqData.mAvg_dt
                                        * (frqData.mCount - 1) + dt)
                                        / (float) frqData.mCount;

                                // 時間差と平均値との差の大きさを毎回加算する ->
                                // バラツキが大きいほどmDeviの値が大きくなる
                                frqData.mDev_avg += Math.abs(frqData.mAvg_dt
                                        - dt);
                                if (Math.abs(dt - frqData.mAvg_dt) > frqData.mAvg_dt * 2)
                                    frqData.mErrorCount++;

                                frqData.mLastCountTime = mTime;
                                // frqData.mLastCountValue = value1;
                            }
                            frqData.mAtack = 1;

                        } else if (value1 < frqData.mAvg * 0.9) {

                            frqData.mAtack = 0;
                        }
                        if (add > 0) {
                            frqData.mTotalAdd += add;
                        }
                        if (frqData.mErrorCount > 10) {
                            frqData.mErrorCount = 0;
                            frqData.mCount = 0;
                            frqData.mBaseTime = mTime;
                            frqData.mLastCountTime = mTime;
                            frqData.mAvg_dt = 0;
                            frqData.mDev_avg = 0;
                            frqData.mAtack = 0;
                            frqData.mTotalAdd = 0;
                        }

                    }
                }
                mFFTData = Arrays.copyOf(fftData, fftData.length);

                for (int i = 0; i < mFrqInfoList.size(); i++) {
                    FrqInfo info = mFrqInfoList.get(i);
                    if (info != null) {
                        int dn = info.mFrqWidth;
                        int index = frqToIndex(info.mFrq, n / 2);
                        float value = 0;
                        for (int j = index - dn; j <= index + dn; j++) {
                            if ((j >= 1) && (j < n / 2)) {
                                byte rvale = fftData[2 * j]; // 実部
                                byte ivalue = fftData[2 * j + 1]; // 虚部
                                float _value = (float) Math.sqrt(rvale * rvale
                                        + ivalue * ivalue) / 255;
                                if (_value > value)
                                    value = _value;
                            }
                        }
                        // ある程度の大きさの音が鳴ったら、鳴った時刻を記憶しておく
                        if (value > info.mTh * getThresholdGain()) {
                            if (info.mResul != 1) {
                                float vol = (float) getCurrentVol();
                                if (vol > 0)
                                    value = (float) mMaxVol * value / vol;
                                info.mAttackTimelist.add(new AttackTime(mTime,
                                        value));
                            }
                            info.mResul = 1;
                        } else {
                            info.mResul = 0;
                        }
                    }
                }
            }
            mVisualizerView.updateVisualizerBms(mFrqInfoList, mTime);
        } else {
            sumcount++;
            if (!isEnableVisual()) {
                return;
            }
            // Logger.d("samplingRate="+samplingRate);
            /*
             * double minDBValue = -90; double maxDBValue = 0; double dbScale =
             * (maxDBValue - minDBValue);
             */

            // これで1024が512になる
            float[] dblist = new float[fftData.length / 2];
            // ２点のデータから対数を計算する
            for (int i = 0; i < fftData.length / 2; i++) {
                /*
                 * byte rfk = bytes[2 * i]; byte ifk = bytes[2 * i + 1]; float
                 * magnitude = (float) (rfk * rfk + ifk * ifk);
                 */
                float magnitude = fftData[2 * i] * fftData[2 * i]
                        + fftData[2 * i + 1] * fftData[2 * i + 1];
                int dbValue = (int) (10 * Math.log10(magnitude));
                dblist[i] = (float) (dbValue * 7.0f);
            }

            int pos = N - (int) (sumcount % N) - 1;
            System.arraycopy(dblist, 0, sumsf[pos], 0, sumsf[pos].length);
            if (sumcount > N) {
                for (int i = 0; i < dblist.length; i++) {
                    float max = 0;
                    for (int j = 0; j < N; j++) {
                        if (max < sumsf[j][i]) {
                            max = sumsf[j][i];
                        }
                    }
                    actf[i] = max;
                }
                mVisualizerView.updateVisualizer(actf);
            }
        }
    }

    // ボリューム取得
    public int getCurrentVol() {
        int ret = 0;
        if (audiomgr != null) {
            ret = audiomgr.getStreamVolume(AudioManager.STREAM_MUSIC);
        }
        return ret;
    }

    // 周波数 -> データ配列のインデックス
    private int frqToIndex(float frq, int num) {
        if (mSamplingRate > 0)
            return (int) (num * frq / (mSamplingRate / 2.0f));
        return -1;
    }

    // ゲイン
    private float getThresholdGain() {
        float ret = mThGain;
        if (mMaxVol > 0)
            ret *= (float) getCurrentVol() / (float) mMaxVol;
        if (ret < 0.01f)
            ret = 0.01f;
        return ret;
    }

    void reloadBand() {
        if (seekBands != null) {
            int bands;
            try {
                bands = getBinder().getNumberOfBands();
                for (int i = 0; i < bands; i++) {
                    int x = getBinder().getBandLevel(i);
                    seekBands[i].setProgress(x + Math.abs(minEQLevel));
                    textCurHz[i].setText(Integer.toString(x));
                }
            } catch (RemoteException e) {
            }
        }
    }

    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_visual, container, false);

        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        mModeFft = mPref.getInt(SystemConsts.PREF_VISUALIZER_MODE,
                VisualMode.FFT);

        mFrqInfoList = new ArrayList<FrqInfo>();
        mFrqInfoList.add(new FrqInfo(32.0f, 0.5f, 2));
        mFrqInfoList.add(new FrqInfo(64.0f, 0.5f, 2));
        mFrqInfoList.add(new FrqInfo(125.0f, 0.5f, 2));
        mFrqInfoList.add(new FrqInfo(250.0f, 0.5f, 2));
        mFrqInfoList.add(new FrqInfo(500.0f, 0.5f, 2));
        mFrqInfoList.add(new FrqInfo(1000.0f, 0.5f, 2));
        mFrqInfoList.add(new FrqInfo(2000.0f, 0.5f, 2));
        mFrqInfoList.add(new FrqInfo(4000.0f, 0.5f, 2));
        mFrqInfoList.add(new FrqInfo(8000.0f, 0.5f, 2));
        mFrqInfoList.add(new FrqInfo(16000.0f, 0.5f, 2));

        mVisualizerView = (AudioVisualizer) root.findViewById(R.id.audioVisualizer1);
        mVisualizerView.setOnTouchListener(this);

        seekLayout = (LinearLayout) root.findViewById(R.id.seekLayout);


        textCurVol = (TextView) root.findViewById(R.id.textCurVol);
        textCurBass = (TextView) root.findViewById(R.id.textCurBass);
        textCurVirtual = (TextView)root.findViewById(R.id.textCurVirtual);
        seekVolume = (VerticalSeekBar) root.findViewById(R.id.seekVolume);
        chkEqOn = (CheckBox) root.findViewById(R.id.chkEqOn);
        chkRvOn = (CheckBox) root.findViewById(R.id.chkRvOn);
        chkBsOn = (CheckBox) root.findViewById(R.id.chkBsOn);
        chkVrOn = (CheckBox) root.findViewById(R.id.chkVrOn);
        spinPreset = (Spinner) root.findViewById(R.id.spinPreset);
        spinReverb = (Spinner) root.findViewById(R.id.spinRvPreset);
        seekBassStrengs = (VerticalSeekBar) root.findViewById(R.id.seekBsPreset);
        seekVirtualStrengs = (VerticalSeekBar)root.findViewById(R.id.seekVrPreset);
        spinPreset.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3) {
                try {
                    if (getBinder() != null) {
                        int current = getBinder().getCurrentPreset();
                        if (current != (arg2 - 1)) {
                            getBinder().usePreset(arg2 - 1);
                            reloadBand();
                        }
                    }
                } catch (RemoteException e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        spinReverb.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3) {
                try {
                    if (getBinder() != null) {
                        int select = REVERB_PRESET[arg2];
                        int current = getBinder().getRvPreset();
                        if (current != select) {
                            getBinder().setRvPreset(select);
                        }
                    }
                } catch (RemoteException e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        chkEqOn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getBinder().setEqEnabled(chkEqOn.isChecked());
                } catch (RemoteException e) {

                }
            }
        });
        chkRvOn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getBinder().setRvEnabled(chkRvOn.isChecked());
                } catch (RemoteException e) {

                }
            }
        });
        chkBsOn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getBinder().setBsEnabled(chkBsOn.isChecked());
                } catch (RemoteException e) {

                }
            }
        });
        
        chkVrOn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    getBinder().setVrEnabled(chkVrOn.isChecked());
                } catch (RemoteException e) {

                }
            }
        });

        audiomgr = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        mMaxVol = audiomgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int progress = audiomgr.getStreamVolume(AudioManager.STREAM_MUSIC);
        seekVolume.setMax(mMaxVol);
        seekVolume.setProgress(progress);
        textCurVol.setText(Integer.toString(progress));
        seekVolume.setOnSeekBarChangeListener(this);

        seekBassStrengs.setMax(1000);
        seekBassStrengs.setProgress(0);
        textCurBass.setText(Integer.toString(0));
        seekBassStrengs.setOnSeekBarChangeListener(this);
        
        seekVirtualStrengs.setMax(1000);
        seekVirtualStrengs.setProgress(0);
        textCurVirtual.setText(Integer.toString(0));
        seekVirtualStrengs.setOnSeekBarChangeListener(this);
        
        mGestureDetector = new GestureDetector(getActivity(), this);
        if (savedInstanceState != null) {
            mModeFft = savedInstanceState.getInt(EQUALIZER_MODE_KEY,
                    VisualMode.FFT);
        }

        return root;
    }

    private void createSeekBar(IMediaPlayerService binder) {
        LayoutInflater inflator = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int bands;
        try {
            releaseBar();
            bands = binder.getNumberOfBands();
            trackview = new View[bands];
            textCurHz = new TextView[bands];
            textBands = new TextView[bands];
            seekBands = new VerticalSeekBar[bands];
            for (int i = 0; i < bands; i++) {
                trackview[i] = inflator.inflate(R.layout.eq_seek_view,
                        seekLayout, false);
                seekLayout.addView(trackview[i]);
                textCurHz[i] = (TextView) trackview[i]
                        .findViewById(R.id.textCurHz);
                textBands[i] = (TextView) trackview[i]
                        .findViewById(R.id.textHz);
                seekBands[i] = (VerticalSeekBar) trackview[i]
                        .findViewById(R.id.seekHz);
                seekBands[i].setOnSeekBarChangeListener(this);
            }
        } catch (RemoteException e) {
        }
    }

    private void releaseBar() {
        if (trackview != null) {
            for (int i = 0; i < trackview.length; i++) {
                if (trackview[i] != null) {
                    seekLayout.removeView(trackview[i]);
                }
            }
            trackview = null;
        }
    }

    @Override
    public void onProgressChanged(VerticalSeekBar seekBar, int progress,
            boolean fromUser) {
        int num = -1;
        if (seekBar.getId() == R.id.seekVolume) {
            textCurVol.setText(Integer.toString(seekBar.getProgress()));
            return;
        } else if (seekBar.getId() == R.id.seekBsPreset) {
            textCurBass.setText(Integer.toString(seekBar.getProgress()));
            return;
        } else if (seekBar.getId() == R.id.seekVrPreset){
            textCurVirtual.setText(Integer.toString(seekBar.getProgress()));
            return;
        }
        for (int i = 0; i < seekBands.length; i++) {
            if (seekBar == seekBands[i]) {
                num = i;
                break;
            }
        }
        if (num != -1) {
            int value = seekBar.getProgress() - Math.abs(minEQLevel);
            textCurHz[num].setText(Integer.toString(value));
        }
    }

    @Override
    public void onStartTrackingTouch(VerticalSeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(VerticalSeekBar seekBar) {
        int num = -1;
        if (seekBar.getId() == R.id.seekVolume) {
            textCurVol.setText(Integer.toString(seekBar.getProgress()));
            audiomgr.setStreamVolume(AudioManager.STREAM_MUSIC,
                    seekBar.getProgress(), 0);
            return;
        } else if (seekBar.getId() == R.id.seekBsPreset) {
            int value = seekBar.getProgress();
            try {
                textCurBass.setText(Integer.toString(seekBar.getProgress()));
                getBinder().setStrength(value);
            } catch (RemoteException e) {
            }
            return;
        }else if (seekBar.getId() == R.id.seekVrPreset){
            int value = seekBar.getProgress();
            try{
                textCurVirtual.setText(Integer.toString(seekBar.getProgress()));
                getBinder().setVirtual(value);
            } catch(RemoteException e){
            }
        }
        for (int i = 0; i < seekBands.length; i++) {
            if (seekBar == seekBands[i]) {
                num = i;
                break;
            }
        }
        if (num != -1) {
            spinPreset.setSelection(0);
            int value = seekBar.getProgress() - Math.abs(minEQLevel);
            textCurHz[num].setText(Integer.toString(value));
            try {
                getBinder().setBandLevel(num, value);
            } catch (RemoteException e) {
            }
        }
    }

    public void reload() {
        IMediaPlayerService binder = getBinder();
        if (binder != null) {
            try {
                mAudioSessionId = binder.getAudioSessionId();
                if (mAudioSessionId != -1) {
                    setEqualizer(binder);
                }
            } catch (RemoteException e) {
            }
        }
    }

    @Override
    public void release() {
    }

    @Override
    public void changedMedia() {
        IMediaPlayerService binder = getBinder();
        clearBmsData();
        if (binder != null) {
            int sessionid;
            try {
                sessionid = binder.getAudioSessionId();
                if (mVisualizer == null || mAudioSessionId != sessionid) {
                    mAudioSessionId = sessionid;
                    if (mAudioSessionId != -1) {
                        bind();
                    }
                }
            } catch (RemoteException e) {
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (mGestureDetector.onTouchEvent(event)) {
            return true;
        }
        return v.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent arg0) {
        return true;
    }

    @Override
    public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2,
            float arg3) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent arg0) {
        if (mVisualizer != null) {
            mVisualizer.setEnabled(false);
            mVisualizer.release();
            mVisualizer = null;
        }
        mModeFft = getNextFftMode(mModeFft);
        Editor edit = mPref.edit();
        edit.putInt(SystemConsts.PREF_VISUALIZER_MODE, mModeFft);
        edit.commit();
        bind();
    }

    private int getNextFftMode(int modeFft) {
        switch (modeFft) {
            case VisualMode.FFT:
                return VisualMode.WAVE;
            case VisualMode.WAVE:
                return VisualMode.BMS;
            default:
                return VisualMode.FFT;
        }
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
            float distanceY) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public String getName(Context context) {
        return null;
    }

    public void onServiceConnected(final IMediaPlayerService binder)  {
        try {
            mAudioSessionId = binder.getAudioSessionId();
            if (mAudioSessionId != -1) {
                setEqualizer(binder);
                bind();
            }
        } catch (RemoteException e) {
            Logger.e("セッションID取得時エラー", e);
        }
    }

}
