package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import android.app.ActionBar.Tab;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseFragment;
import jp.co.kayo.android.localplayer.MainActivity2;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainFragment extends Fragment implements ContentManager, ContextMenuFragment,
        IProgressView {
    SharedPreferences mPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(FragmentUtils.getLayoutId(getActivity(), this), null);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        int position = FragmentUtils.getTabPosition(this);
        MainActivity2 activity = (MainActivity2) getActivity();
        Tab tab = activity.getSupportActionBar().getTabAt(position);

        String tabname = null;
        Fragment current = getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (current == null) {
            tabname = selectView(getActivity(), FragmentUtils.getTabKind(getActivity(), this));
        } else {
            if(!current.isVisible()){
                tabname = selectView(getActivity(), FragmentUtils.getTabKind(getActivity(), this));
            }
            else{
                if (current instanceof ContentManager) {
                    tabname = ((ContentManager) current).getName(getActivity());
                }
                else {
                    tabname = getString(R.string.lb_tab_blank);
                }
            }
        }

        activity.mTabsAdapter.setTabText(tab, tabname);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public String selectView(Context context, int kind) {
        Fragment f = null;
        if (kind == SystemConsts.TAG_ALBUM_GRID) {
            f = new AlbumGridViewFragment();
        }
        else if (kind == SystemConsts.TAG_ALBUM_LIST) {
            f = new AlbumListViewFragment();
        }
        else if (kind == SystemConsts.TAG_ARTIST_LIST) {
            f = new ArtistListViewFragment();
        }
        else if (kind == SystemConsts.TAG_ARTIST_EXPAND) {
            f = new ArtistExpandViewFragment();
        }
        else if (kind == SystemConsts.TAG_MEDIA) {
            f = new MediaListViewFragment();
        }
        else if (kind == SystemConsts.TAG_PLAYLIST) {
            f = new PlaylistViewFragment();
        }
        else if (kind == SystemConsts.TAG_FOLDER) {
            f = new FileListViewFragment();
        }
        else if (kind == SystemConsts.TAG_GENRES) {
            f = new GenresListViewFragment();
        }
        else if (kind == SystemConsts.TAG_VIDEO) {
            f = new VideoListViewFragment();
        }
        else if (kind == SystemConsts.TAG_PLAYBACK) {
            f = new PlaybackListViewFragment();
        }
        else {
            f = new QuickHelpFragment();
            kind = SystemConsts.TAG_BLANK;
        }
        FragmentUtils.saveTabKind(context, this, kind);

        if (f != null) {
            Fragment old = currentFragment();
            if(old != null && old instanceof ContextMenuFragment){
                ContextMenuFragment menufragment = (ContextMenuFragment)old;
                menufragment.hideMenu();
            }
            
            Bundle args = FragmentUtils.cloneBundle(this);
            f.setArguments(args);
            
            getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            
            FragmentTransaction t = getFragmentManager().beginTransaction();
            t.replace(FragmentUtils.getFragmentId(context, this), f);
            t.commit();

            if (f instanceof ContentManager) {
                return ((ContentManager) f).getName(context);
            }
        }
        return context.getString(R.string.lb_tab_blank);
    }
    
    public Fragment currentFragment(){
        return getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
    }

    @Override
    public void reload() {
        ContentManager mgr = (ContentManager) getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (mgr != null) {
            Logger.d("Reload Class=" + mgr.getClass().getName());
            mgr.reload();
        }
    }

    @Override
    public void changedMedia() {
        ContentManager mgr = (ContentManager) getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (mgr != null) {
            mgr.changedMedia();
        }
    }

    @Override
    public void release() {
        ContentManager mgr = (ContentManager) getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (mgr != null) {
            mgr.release();
        }
    }

    @Override
    public String getName(Context context) {
        ContentManager mgr = (ContentManager) getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (mgr != null) {
            return mgr.getName(context);
        }
        return null;
    }

    @Override
    public boolean onBackPressed() {
        Fragment f = getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (f instanceof ContextMenuFragment) {
            return ((ContextMenuFragment) f).onBackPressed();
        }
        return false;
    }
    
    @Override
    public void hideMenu() {
        Fragment f = getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (f instanceof ContextMenuFragment) {
            ContextMenuFragment cm = (ContextMenuFragment) f;
            cm.hideMenu();
        }
    }

    @Override
    public String selectSort() {
        Fragment f = getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (f instanceof ContextMenuFragment) {
            ContextMenuFragment menufragment = (ContextMenuFragment)f;
            menufragment.hideMenu();
            return menufragment.selectSort();
        }
        return null;
    }

    @Override
    public void doSearchQuery(String queryString) {
        Fragment f = getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (f instanceof ContextMenuFragment) {
            ContextMenuFragment menufragment = (ContextMenuFragment)f;
            menufragment.hideMenu();
            menufragment.doSearchQuery(queryString);
        }
    }

    @Override
    public void startProgress(long max) {
        Fragment f = getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (f instanceof IProgressView) {
            ((IProgressView) f).startProgress(max);
        }
    }

    @Override
    public void stopProgress() {
        Fragment f = getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (f instanceof IProgressView) {
            ((IProgressView) f).stopProgress();
        }
    }

    @Override
    public void progress(long pos, long max) {
        Fragment f = getFragmentManager().findFragmentById(
                FragmentUtils.getFragmentId(getActivity(), this));
        if (f instanceof IProgressView) {
            ((IProgressView) f).progress(pos, max);
        }
    }

    @Override
    public int getFragmentId() {
        Fragment f = currentFragment();
        if(f != null && f instanceof ContextMenuFragment){
            return ((ContextMenuFragment)f).getFragmentId();
        }
        return -1;
    }
}
