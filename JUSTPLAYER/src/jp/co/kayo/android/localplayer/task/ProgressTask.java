package jp.co.kayo.android.localplayer.task;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.dialog.ProgressFragment;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

abstract public class ProgressTask extends AsyncTask<Void, Void, Void>{
    protected ProgressFragment dialog;
    private FragmentManager fm;
    
    public ProgressTask(FragmentManager fm){
        this.fm = fm;
    }
    
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Fragment f = fm.findFragmentByTag("progress_dialog");
        if(f!=null){
            FragmentTransaction ft = fm.beginTransaction();
            ft.remove(f);
            ft.commit();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressFragment();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(dialog, "progress_dialog");
        ft.commit();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Fragment f = fm.findFragmentByTag("progress_dialog");
        if(f!=null){
            FragmentTransaction ft = fm.beginTransaction();
            ft.remove(f);
            ft.commit();
        }
    }
}
