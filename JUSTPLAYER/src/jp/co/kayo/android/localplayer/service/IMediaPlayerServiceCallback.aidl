package jp.co.kayo.android.localplayer.service;

oneway interface IMediaPlayerServiceCallback {
	void updateView(boolean updatelist);
	void updateList();
	void onBufferingUpdate(int percent);
	
    void startProgress(long max);
    void stopProgress();
    void progress(long pos, long max);
    void close();
}
