package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v4.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

public class DeletePlaylistDialog extends DialogFragment implements
        LoaderCallbacks<Cursor>, OnClickListener {
    private DeletePlaylistListAdapter mAdapter;
    private ListView mListView;

    public static class DeleteItems {
        String name;
        long id;
        boolean checked;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.playlist_grid_view, null, false);
        mListView = (ListView) view.findViewById(android.R.id.list);
        // フォーカスが当たらないよう設定
        mListView.setItemsCanFocus(false);
        // 選択の方式の設定
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        mListView.setFastScrollEnabled(false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.playlist_delete_title));
        builder.setPositiveButton(getString(R.string.lb_ok), this);
        builder.setNegativeButton(getString(R.string.lb_cancel), null);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getLoaderManager().initLoader(R.layout.playlist_grid_view, null, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(R.layout.playlist_grid_view);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        mAdapter = new DeletePlaylistListAdapter(getActivity(),
                new ArrayList<DeleteItems>());
        mListView.setAdapter(mAdapter);

        return new CursorLoader(getActivity(),
                MediaConsts.PLAYLIST_CONTENT_URI, null, null, null,
                MediaConsts.AudioPlaylist.DATE_ADDED);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null && data.moveToFirst()) {
            do {
                DeleteItems item = new DeleteItems();
                item.id = data.getLong(data.getColumnIndex(AudioPlaylist._ID));
                item.name = data.getString(data
                        .getColumnIndex(MediaConsts.AudioPlaylist.NAME));
                item.checked = false;
                mAdapter.add(item);
            } while (data.moveToNext());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        final SparseBooleanArray checked = mListView.getCheckedItemPositions();
        if (checked != null && checked.size() > 0) {
            final ContentResolver resolver = getActivity().getContentResolver();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getString(R.string.lb_confirm));
            builder.setMessage(String.format(
                    getString(R.string.playlist_delete_confirm),
                    Integer.toString(checked.size())));
            builder.setPositiveButton(getString(R.string.lb_ok),
                    new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // delte proc
                            for (int i = 0; i < checked.size(); i++) {
                                // チェックされているアイテムの key の取得
                                int key = checked.keyAt(i);
                                DeleteItems item = mAdapter.getItem(key);
                                resolver.delete(
                                        ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, item.id),
                                        null, null);
                                resolver.delete(
                                        MediaConsts.PLAYLIST_CONTENT_URI,
                                        AudioPlaylist._ID + " = ?", new String[]{Long.toString(item.id)});
                            }
                        }
                    });
            builder.setNegativeButton(getString(R.string.lb_cancel), null);
            builder.setCancelable(true);
            AlertDialog dlg = builder.create();
            dlg.show();
        }
    }
}
