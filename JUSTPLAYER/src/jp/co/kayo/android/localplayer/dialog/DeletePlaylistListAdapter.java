package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.dialog.DeletePlaylistDialog.DeleteItems;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

public class DeletePlaylistListAdapter extends ArrayAdapter<DeleteItems> {
    LayoutInflater inflator;
    Context context;
    Handler handler = new Handler();

    public DeletePlaylistListAdapter(Context context,
            ArrayList<DeleteItems> items) {
        super(context, -1, items);
        this.context = context;
    }

    public LayoutInflater getInflator(Context context) {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = getInflator(context).inflate(
                    R.layout.playlist_check_row, parent, false);
            holder = new ViewHolder();
            holder.text1 = (CheckedTextView) convertView
                    .findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        DeleteItems item = getItem(position);

        holder.text1.setText(item.name);
        holder.text1.setChecked(item.checked);

        return convertView;
    }

    private class ViewHolder {
        CheckedTextView text1;
    }
}
