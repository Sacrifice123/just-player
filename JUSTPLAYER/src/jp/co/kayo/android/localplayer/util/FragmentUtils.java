package jp.co.kayo.android.localplayer.util;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;

public class FragmentUtils {
    public static Bundle cloneBundle(Fragment f){
        Bundle args = new Bundle();
        if(f.getArguments()!=null){
            args.putInt("tabposition", f.getArguments().getInt("tabposition"));
            args.putString("tagname", f.getArguments().getString("tagname"));
        }
        return args;
    }
    
    public static void saveTabKind(Context context, Fragment f, int kind){
        if(f!=null){
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            Editor editor = pref.edit();
            editor.putInt("tag.kind"+getTabPosition(f), kind);
            editor.commit();
        }
    }
    
    public static int getTabKind(Context context, Fragment f){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        int pos = getTabPosition(f);
        if(pos >= 0){
            if(pos == 0){
                return pref.getInt("tag.kind"+getTabPosition(f), SystemConsts.TAG_ALBUM_GRID);
            }else if(pos == 1){
                return pref.getInt("tag.kind"+getTabPosition(f), SystemConsts.TAG_PLAYBACK);
            }
            else{
                return pref.getInt("tag.kind"+getTabPosition(f), SystemConsts.TAG_BLANK);
            }
        }
        return -1;
    }
    
    public static int getTabPosition(Fragment f){
        Bundle args = f.getArguments();
        if(args!=null){
            return args.getInt("tabposition");
        }
        return -1; 
    }
    
    public static int getLayoutId(Context context, Fragment f){
        int pos = getTabPosition(f);
        if(pos !=-1){
            return context.getResources().getIdentifier("fragment_main"+(pos+1), "layout", "jp.co.kayo.android.localplayer");
        }
        else{
            return R.layout.fragment_main1;
        }
    }
    
    public static int getFragmentId(Context context, Fragment f){
        int pos = getTabPosition(f);
        if(pos !=-1){
            return context.getResources().getIdentifier("fragment_main"+(pos+1), "id", "jp.co.kayo.android.localplayer");
        }
        else{
            return R.id.fragment_main1;
        }
    }

}
