package jp.co.kayo.android.localplayer.util;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;

public class AnimationHelper {

    public static void setViewToPlayback(View view, AnimationListener listner) {
        // 移動アニメーション
        TranslateAnimation trans = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.4F,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -0.8f);
        trans.setDuration(800);
        // 縮小アニメーション
        ScaleAnimation scale = new ScaleAnimation(
                1.0f, 0.0f,
                1.0f, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scale.setStartOffset(100);
        scale.setDuration(400);

        // アニメーションセットを作成
        AnimationSet set = new AnimationSet(false);
        set.setFillAfter(false);
        set.setFillEnabled(true);
        set.addAnimation(scale);
        set.addAnimation(trans);
        set.setAnimationListener(listner);
        view.startAnimation(set);
    }
    
    
    public static void setFragmentToPlayBack(FragmentTransaction t){
        t.setCustomAnimations(R.anim.pull_list_in, R.anim.alpha_hide, R.anim.pull_list_in, R.anim.alpha_hide);
    }

}
