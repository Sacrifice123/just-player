package jp.co.kayo.android.localplayer.provider;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class UpdateAlbumSongsProc implements Runnable {
    SQLiteDatabase db;
    String[] projection;
    String album_key;
    ArrayList<ArrayList<Object>> songs;

    public UpdateAlbumSongsProc(SQLiteDatabase db, String album_key,
            String[] projection, ArrayList<ArrayList<Object>> songs) {

        this.db = db;
        this.projection = projection;
        this.album_key = album_key;
        this.songs = songs;
    }

    @Override
    public void run() {
        for (ArrayList<Object> values : songs) {
            Cursor cur = null;
            try {
                cur = db.query(TableConsts.TBNAME_AUDIO,
                        new String[] { BaseColumns._ID }, AudioMedia.MEDIA_KEY
                                + "=?",
                        new String[] { (String) values.get(0) }, null, null,
                        null);
                if (cur != null && cur.getCount() > 0) {

                } else {
                    ContentValues val = new ContentValues(projection.length);
                    for (int i = 0; i < projection.length; i++) {
                        Object o = values.get(i);
                        if (projection.equals(BaseColumns._ID)) {

                        } else if (o instanceof String) {
                            val.put(projection[i], (String) o);
                        } else if (o instanceof Integer) {
                            val.put(projection[i], (Integer) o);
                        } else if (o instanceof Long) {
                            val.put(projection[i], (Long) o);
                        } else if (o instanceof Boolean) {
                            val.put(projection[i], (Boolean) o);
                        }
                    }
                    db.insert(TableConsts.TBNAME_AUDIO, null, val);
                }
            } finally {
                if (cur != null) {
                    cur.close();
                }
            }
        }

        Cursor cur = null;
        try {
            cur = db.query(TableConsts.TBNAME_ALBUM,
                    new String[] { BaseColumns._ID }, AudioAlbum.ALBUM_KEY
                            + "=?", new String[] { album_key }, null, null,
                    null);
            if (cur != null && cur.getCount() > 0) {
                ContentValues val = new ContentValues();
                val.put(TableConsts.ALBUM_INIT_FLG, 1);
                db.update(TableConsts.TBNAME_ALBUM, val, AudioAlbum.ALBUM_KEY
                        + "=?", new String[] { album_key });
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

}
