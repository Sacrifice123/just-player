package jp.co.kayo.android.localplayer.plugin;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

public class ColorTemperture {
    float KelvinLnToX(float x) {
        return (-0.0108f * x * x * x + 0.3407f * x * x - 3.5987f * x + 12.929f);
    }

    float KelvinToX(float K) {
        return KelvinLnToX((float) Math.log(K));
    }

    float XToY(float X) {
        return -2.700931324f * X * X + 2.675633166f * X - 0.250545382f;
    }

    long GetColor(float K, float Y) {
        RColor Yxy = new RColor();
        Yxy.Y = Y;
        Yxy.x = KelvinToX(K);
        Yxy.y = XToY(Yxy.x);
        return YxyToRGB32(Yxy);
    }

    long GetColorLn(float K, float Y) {
        RColor Yxy = new RColor();
        Yxy.Y = Y;
        Yxy.x = KelvinLnToX(K);
        Yxy.y = XToY(Yxy.x);
        return YxyToRGB32(Yxy);
    }

    void RGBToYxy(RColor rgb, RColor Yxy) {
        float X = 0.412453f * rgb.R + 0.35758f * rgb.G + 0.180423f * rgb.B;
        Yxy.Y = 0.212671f * rgb.R + 0.71516f * rgb.G + 0.072169f * rgb.B;
        float Z = 0.019334f * rgb.R + 0.119193f * rgb.G + 0.950227f * rgb.B;
        Yxy.x = X / (X + Yxy.Y + Z);
        Yxy.y = Yxy.Y / (X + Yxy.Y + Z);
    }

    void YxyToRGB(RColor Yxy, RColor rgb) {
        float X = Yxy.x / Yxy.y * Yxy.Y;
        float Z = (1.0f - Yxy.x - Yxy.y) / Yxy.y * Yxy.Y;
        rgb.R = 3.240479f * X - 1.53715f * Yxy.Y - 0.498535f * Z;
        rgb.G = -0.969256f * X + 1.875991f * Yxy.Y + 0.041556f * Z;
        rgb.B = 0.055648f * X - 0.204043f * Yxy.Y + 1.057311f * Z;
    }

    long YxyToRGB32(RColor Yxy) {
        RColor rgb = new RColor();
        YxyToRGB(Yxy, rgb);
        // 各値を0 - 255にする
        int r = (int) (rgb.R * 255f);
        int g = (int) (rgb.G * 255f);
        int b = (int) (rgb.B * 255f);
        r = r >= 255 ? 255 : r <= 0 ? 0 : r;
        g = g >= 255 ? 255 : g <= 0 ? 0 : g;
        b = b >= 255 ? 255 : b <= 0 ? 0 : b;
        return ((r << 16) | (g << 8) | b);
    }

    class RColor {
        float Y;
        float x;
        float y;
        float R;
        float G;
        float B;
    }
}
