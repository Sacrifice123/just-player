package jp.co.kayo.android.localplayer.menu;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView.MultiChoiceModeListener;

abstract public class MultipleChoiceMediaContentActionCallback implements Callback, MultiChoiceModeListener {
    protected Context mContext;
    protected List<Integer> mSelectedItems;
    protected Handler mHandler;

    public MultipleChoiceMediaContentActionCallback(Context context, List<Integer> selectedItems,
            Handler handler) {
        mContext = context;
        mSelectedItems = selectedItems;
        mHandler = handler;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        int what = -1;
        if (item.getTitle().equals(mContext.getString(R.string.sub_mnu_rating))) {
            what = SystemConsts.EVT_SELECT_RATING;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_play))) {
            what = SystemConsts.EVT_SELECT_PLAY;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_order))) {
            what = SystemConsts.EVT_SELECT_ADD;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_album))) {
            what = SystemConsts.EVT_SELECT_OPENALBUM;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.txt_web_more))) {
            what = SystemConsts.EVT_SELECT_MORE;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_albumart))) {
            what = SystemConsts.EVT_SELECT_ALBUMART;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_download))) {
            what = SystemConsts.EVT_SELECT_DOWNLOAD;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_clearcache))) {
            what = SystemConsts.EVT_SELECT_CLEARCACHE;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.txt_web_lyrics))) {
            what = SystemConsts.EVT_SELECT_LYRICS;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.txt_web_youtube))) {
            what = SystemConsts.EVT_SELECT_YOUTUBE;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_love))) {
            what = SystemConsts.EVT_SELECT_LOVE;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_ban))) {
            what = SystemConsts.EVT_SELECT_BAN;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_remove))) {
            what = SystemConsts.EVT_SELECT_DEL;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_artist))) {
            what = SystemConsts.EVT_SELECT_ARTIST;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_edit))) { // 文字化けメニュー
            what = SystemConsts.EVT_SELECT_EDIT;
        }

        Message msg =  mHandler.obtainMessage(what, mSelectedItems);
        mHandler.sendMessage(msg);
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        setActionMode(null);
        mSelectedItems.clear();
    }
    
    abstract public void setActionMode(ActionMode mode);

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
        setActionMode(mode);
        if(checked){
            if(!mSelectedItems.contains(position)){
                mSelectedItems.add(position);
            }
        }else{
            if(mSelectedItems.contains(position)){
                mSelectedItems.remove(Integer.valueOf(position));
            }
        }
        
        if(mContext instanceof ContentManager){
            mode.setTitle(((ContentManager)mContext).getName(mContext));
        }else{
            mode.setTitle(mContext.getString(R.string.txt_selected_title));
        }
        mode.setSubtitle(String.format(mContext.getString(R.string.fmt_selected_items), mSelectedItems.size())); 
        
    }
}
