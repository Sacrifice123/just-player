package jp.co.kayo.android.localplayer.appwidget;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

public class ColorPickerDialog extends ListActivity {

    ColorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.colorpicker);
        
        ArrayList<ColorInfo> list;
        try {
            list = readFile("genshoku.txt");
            adapter = new ColorAdapter(this, list);

            getListView().setAdapter(adapter);
        } catch (IOException e) {
            e.printStackTrace();
        }

        getListView().setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                    long arg3) {
                final Intent intent = new Intent();
                ColorInfo item = adapter.getItem(pos);
                intent.putExtra(Intent.ACTION_PICK, item.icolor);
                intent.putExtra("name", item.name);
                intent.putExtra("color", getColorString(item.icolor));
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }
    
    private ArrayList<ColorInfo> readFile(String fname) throws IOException {
        ArrayList<ColorInfo> list = new ArrayList<ColorInfo>();
        //defaultの追加
        list.add(new ColorInfo());
        
        BufferedInputStream bs = null;
        try {
            bs = new BufferedInputStream(getAssets().open(fname));

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            while (true) {
                int c = bs.read();
                if (c == -1) {
                    String s = out.toString();
                    if (s.length() > 0) {
                        StringBuilder typename = new StringBuilder();
                        if (fname.indexOf("bibit") >= 0) typename.append("BIBIT");
                        else if (fname.indexOf("pastel") >= 0) typename.append("PASTEL");
                        else if (fname.indexOf("web216") >= 0) typename.append("WEB");
                        typename.append(String.format("%1$03d", list.size() + 1));
                        list.add(new ColorInfo(s, typename.toString()));
                    }
                    break;
                }

                if (c != '\n') {
                    out.write(c);
                    continue;
                } else {
                    String s = out.toString();
                    StringBuilder typename = new StringBuilder();
                    if (fname.indexOf("bibit") >= 0) typename.append("BIBIT");
                    else if (fname.indexOf("pastel") >= 0) typename.append("PASTEL");
                    else if (fname.indexOf("web216") >= 0) typename.append("WEB");
                    typename.append(String.format("%1$03d", list.size() + 1));

                    list.add(new ColorInfo(s, typename.toString()));
                    out = new ByteArrayOutputStream();
                }
            }

            return list;
        } finally {
            if (bs != null) try {
                bs.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public String getColorString(int icolor) {
        int r = Color.red(icolor);
        int g = Color.green(icolor);
        int b = Color.blue(icolor);
        return "#" + toHexString(r, 2) + toHexString(g, 2) + toHexString(b, 2);
    }

    public String toHexString(int value, int n) {
        String s = Integer.toHexString(value);
        StringBuilder sb = new StringBuilder();
        for (int i = s.length(); i < n; i++) {
            sb.append("0");
        }
        sb.append(s);
        return sb.toString().toUpperCase();
    }
    
    class ColorAdapter extends ArrayAdapter<ColorInfo>{

        public ColorAdapter(Context context, List<ColorInfo> list) {
            super(context, android.R.layout.simple_list_item_1, list);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                convertView = super.getView(position, convertView, parent);
            }
            TextView text = (TextView)convertView.findViewById(android.R.id.text1);

            ColorInfo item = getItem(position);

            if(item.icolor!=-1){
                convertView.setBackgroundColor(item.icolor);

                text.setTextSize(14);
                text.setGravity(Gravity.TOP | Gravity.LEFT);

                float[] hsv = new float[3];
                Color.colorToHSV(item.icolor, hsv);

                if (hsv[2] < 0.7) {
                    text.setTextColor(Color.LTGRAY);
                } else {
                    text.setTextColor(Color.DKGRAY);
                }

                text.setText(item.name);
            }else{
                convertView.setBackgroundColor(Color.BLACK);
                text.setTextSize(14);
                text.setGravity(Gravity.TOP | Gravity.LEFT);
                text.setTextColor(Color.LTGRAY);
                text.setText(item.name);
            }

            return convertView;
        }
        
    }
    
    class ColorInfo {
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            if(name!=null && name.length()>0){
                sb.append(name).append(" ");
            }
            
            sb.append(color);
            return sb.toString();
        }

        String name;
        String yomi;
        String color;
        int icolor;
        
        public ColorInfo(){
            name = "default";
            name = "default";
            name = "default";
            icolor = -1;
        }
            
        public ColorInfo(String s, String typename){
            //android color
            name = null;
            yomi = null;
            color = null;
            icolor = 0;
            int isandroid = s.indexOf("Color.");
            if(isandroid!=-1){
                s = s.trim();
                String fname = s.substring("Color.".length());
                Field f;
                try {
                    f = Color.class.getField(fname);
                    int value = (Integer)f.get(null);
                    name = s;
                    icolor = value;
                    int r = Color.red(icolor);
                    int g = Color.green(icolor);
                    int b = Color.blue(icolor);
                    color = "#"+toHexString(r, 2)+toHexString(g, 2)+toHexString(b, 2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                int index = s.indexOf('#');
                String s1 = s.substring(0, index);
                String c = s.substring(index, s.length());
                String r = c.substring(1, 3);
                String g = c.substring(3, 5);
                String b = c.substring(5, 7);
                color = ("#"+r+g+b).toUpperCase();
                icolor = Color.parseColor(color);
                //Logger.d(TAG, s1);
                if(s1.length()>0){
                    name = s1.trim();
                    int c1 = s1.indexOf("(");
                    if(c1!=-1){
                        int c2 = s1.indexOf(")");
                        yomi = s1.substring(c1+1, c2);
                    }
                }
                else{
                    name = typename;
                }
            }
        }
    }
}