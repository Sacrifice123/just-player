
package jp.co.kayo.android.localplayer.appwidget;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.bean.MediaData;

import android.annotation.SuppressLint;
import android.appwidget.AppWidgetManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

@SuppressLint("NewApi")
public class MyWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new MyRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}

class MyRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private Context mContext;
    private Cursor mCursor;
    private int mAppWidgetId;
    private SharedPreferences mPref;
    private final String[] FETCH = new String[]{ AudioMedia._ID, AudioMedia.TITLE, AudioMedia.ARTIST, AudioMedia.ALBUM, AudioMedia.ALBUM_KEY, AudioMedia.DATA, AudioMedia.DURATION};

    public MyRemoteViewsFactory(Context context, Intent intent) {
        mContext = context;
        mPref = PreferenceManager.getDefaultSharedPreferences(context);
        mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {
        if (mCursor != null) {
            mCursor.close();
        }
    }

    @Override
    public int getCount() {
        return mCursor.getCount();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews remoteView = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);
        
        String title = mContext.getString(R.string.txt_unknown_title);
        String artist = mContext.getString(R.string.txt_unknown_artist);
        long duration = 0;
        long id = -1;
        long media_id = -1;
        if (mCursor.moveToPosition(position)) {
            id = mCursor.getLong(mCursor.getColumnIndex(BaseColumns._ID));
            media_id = mCursor.getLong(mCursor.getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID));
            title = mCursor.getString(mCursor.getColumnIndex(TableConsts.PLAYBACK_TITLE));
            artist = mCursor.getString(mCursor.getColumnIndex(TableConsts.PLAYBACK_ARTIST));
            duration = mCursor.getLong(mCursor.getColumnIndex(TableConsts.PLAYBACK_DURATION));
        }
        
        MediaData item = null;
        if(media_id>0){
            item = loadMediaItem(media_id);
        }
        if(item != null){
            title = item.getTitle();
            artist = item.getArtist();
            duration = item.getDuration();
        }
        
        int fontcolor = mPref.getInt(DisplayPreference.KEY_FONTCOLOR,
                DisplayPreference.default_fontcolor);
        remoteView.setTextViewText(R.id.text1, Funcs.getTrack(position + 1));
        remoteView.setTextViewText(R.id.text2, title);
        remoteView.setTextViewText(R.id.text3, artist);
        remoteView.setTextViewText(R.id.text4, Funcs.makeTimeString(duration, false));

        remoteView.setTextColor(R.id.text1, fontcolor);
        remoteView.setTextColor(R.id.text2, fontcolor);
        remoteView.setTextColor(R.id.text3, fontcolor);
        remoteView.setTextColor(R.id.text4, fontcolor);
        
        
        final Intent fillInIntent = new Intent();
        final Bundle extras = new Bundle();
        extras.putLong("EXTRA_PLAYBACK_ID", id);
        fillInIntent.putExtras(extras);
        remoteView.setOnClickFillInIntent(R.id.widget_item, fillInIntent);
        
        return remoteView;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onDataSetChanged() {
        if (mCursor != null) {
            mCursor.close();
        }
        Uri orderuri = Uri.parse(DeviceContentProvider.MEDIA_CONTENT_AUTHORITY_SLASH
                + "order/audio");
        String cur_contenturi = mPref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        mCursor = mContext.getContentResolver().query(orderuri, null,  TableConsts.PLAYBACK_URI + " = ?",
                new String[] {
                    cur_contenturi
                }, TableConsts.PLAYBACK_ORDER);
    }
    
    private MediaData loadMediaItem(long id) {
        Cursor cursor = null;
        try{
            cursor = mContext.getContentResolver().query(
                    ContentUris.withAppendedId(MediaConsts.MEDIA_CONTENT_URI, id), FETCH, null,
                    null, null);

            if(cursor != null && cursor.moveToFirst()){
                MediaData data = new MediaData(
                        0,
                        cursor.getLong(cursor.getColumnIndex(AudioMedia._ID)), 
                        MediaData.NOTPLAYED, 
                        cursor.getLong(cursor.getColumnIndex(AudioMedia.DURATION)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.TITLE)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.ALBUM)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.ARTIST)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.DATA)));
                return data;
            }
        }
        finally{
            if(cursor!=null){
                cursor.close();
            }
        }
        
        return null;
    }

}
