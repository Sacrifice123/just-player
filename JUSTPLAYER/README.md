JUST PLAYER
=========================

This is an open-source project of JUST PLAYER



## Build
Eclipse + ADT
JDK1.6 higher(but Set to Compatible version 1.6)
Android SDK

## External Library
Actionbar Sherlock(Actionbar Compatible Library)
AdView(Ad Project)
LibAndroTranslation(Translation Editor)
id3iconv-0.2.1.dm.jar (Id3d Tag library)
androidexceptionlib.jar ( Exception Report Service Library )

## Secret File

package jp.co.kayo.android.localplayer.secret;

public interface Keys {
    public static final String SIGNATURE_ALGORITHM = "Signature Key";

    public static final String[] HOLD_KEY = { "The key that has been encrypted by Signature Key" };
    
    public static final String EXCEPTION_KEY = "Exception Report Service Key";
}
